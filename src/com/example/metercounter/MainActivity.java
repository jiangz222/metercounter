package com.example.metercounter;


import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class MainActivity extends Activity {
	private EditText edThickness;
	private Spinner spInnerRadius;
	private EditText tvOutterRadius;
	private TextView tvOutPutMeter;
	private float InnerRadius[]={3.75f, 2.75f};
	private int InnerRadiusIdx=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initUI();
	}
	private void initUI(){
		edThickness = (EditText)findViewById(R.id.thickness_input);
		spInnerRadius = (Spinner)findViewById(R.id.inner_radius_input);
		tvOutterRadius = (EditText)findViewById(R.id.outter_radius_input);
		tvOutPutMeter = (TextView)findViewById(R.id.meter_need_output);
		
		edThickness.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
		spInnerRadius.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
				// TODO Auto-generated method stub
				Log.i("metercalc","position; "+position); 
				//This callback is invoked only when the newly selected position is different 
				//from the previously selected position or if there was no selected item.
				if(0 == position)
				{
					InnerRadiusIdx =0;
				}
				if(1 == position)
				{
					InnerRadiusIdx=1;

				}

			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
	
			}
		});
	}
	public void startCalc(View view){
		float fThickness=0f,foutterRadius=0f;
		String sthickness = edThickness.getText().toString().trim();
		if(sthickness == null)
		{
			Toast.makeText(getApplicationContext(), "计算失败，请输入有效厚度（整数或者小数）！",
				     Toast.LENGTH_SHORT).show();	
			return;
		}
		try{
			fThickness = Float.valueOf(sthickness);
		}catch(Exception e){
			Toast.makeText(getApplicationContext(), "计算失败，厚度只能是整数或者小数！",
				     Toast.LENGTH_SHORT).show();
			return;

		}
		
		/*
		String sinnerRadius = tvInnerRadius.getText().toString().trim();
		if(sinnerRadius == null)
		{
			Toast.makeText(getApplicationContext(), "计算失败，请输入内半径（整数或者小数）！",
				     Toast.LENGTH_SHORT).show();		
			return;

		}
		try{
			 finnerRadius = Float.valueOf(sinnerRadius);
		}catch(Exception e){
			Toast.makeText(getApplicationContext(), "计算失败，内半径只是整数或者小数！",
				     Toast.LENGTH_SHORT).show();		
			return;

		}*/
		String soutterRadius = tvOutterRadius.getText().toString().trim();
		if(soutterRadius == null)
		{
			Toast.makeText(getApplicationContext(), "计算失败，请输入外半径（整数或者小数）！",
				     Toast.LENGTH_SHORT).show();		
			return;
		}
		try{
			 foutterRadius = Float.valueOf(soutterRadius);
		}catch(Exception e){
			Toast.makeText(getApplicationContext(), "计算失败，外半径是整数或者小数！",
				     Toast.LENGTH_SHORT).show();		
			return;

		}
		if(foutterRadius<=InnerRadius[InnerRadiusIdx]){
			Toast.makeText(getApplicationContext(), "计算失败，外半径必须大于内半径！",
				     Toast.LENGTH_SHORT).show();		
			return;
		}
		// 开始计算
//		float fThickness,finnerRadius,foutterRadius;

	float numberOfCircle = (float)((foutterRadius-InnerRadius[InnerRadiusIdx])/fThickness);
	int meter = (int)(6.28 *(InnerRadius[InnerRadiusIdx]*numberOfCircle + ((fThickness*(1+numberOfCircle)*numberOfCircle)/2)));	
	meter = meter/100;
	tvOutPutMeter.setText(String.valueOf(meter));	
	tvOutPutMeter.setTextColor(Color.RED);
	Toast.makeText(getApplicationContext(), "计算成功！",
		     Toast.LENGTH_SHORT).show();	
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
